<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //
    public function type(){
        return $this->hasOne(Type::class);
    }

    public function company(){
        return $this->hasOne(Company::class);
    }

    public function log(){
        return $this->belongsToMany(Log::class);
    }

    public function repairement(){
        return $this->hasMany(Repairement::class);
    }

    public function companies_type(){
        return $this->hasMany(Companies_type::class);
    }
}
