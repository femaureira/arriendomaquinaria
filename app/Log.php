<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    //
    public function employee(){
        return $this->hasOne(Employee::class);
    }

    public function client(){
        return $this->hasOne(Client::class);
    }

    public function machine(){
        return $this->hasOne(Machine::class);
    }

    public function type(){
        return $this->hasOne(Type::class);
    }

    public function company(){
        return $this->hasOne(Company::class);
    }

    public function rent(){
        return $this->hasOne(Rent::class);
    }

    public function repairement(){
        return $this->hasOne(Repairement::class);
    }

    public function action(){
        return $this->hasOne(Action::class);
    }
}
