<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    //
    public function employee(){
        return $this->hasMany(Employee::class);
    }

    public function machine(){
        return $this->hasMany(Machine::class);
    }
}
