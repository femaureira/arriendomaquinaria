<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    //
    public function location(){
        return $this->belongsTo(Location::class);
    }

    public function log(){
        return $this->belongsToMany(Log::class);
    }

    public function rent(){
        return $this->belongsToMany(Rent::class);
    }
}
