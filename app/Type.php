<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    //
    public function log(){
        return $this->belongsToMany(Log::class);
    }
    
    public function machine(){
        return $this->hasMany(Machine::class);
    }

    public function companies_type(){
        return $this->hasMany(Companies_type::class);
    }
}
