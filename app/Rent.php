<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rent extends Model
{
    //
    public function client(){
        return $this->hasOne(Client::class);
    }

    public function employee(){
        return $this->hasOne(Employee::class);
    }

    public function machine(){
        return $this->hasOne(Machine::class);
    }

    public function initialState(){
        return $this->hasOne(State::class);
    }
    
    public function endState(){
        return $this->hasOne(State::class);
    }


    public function log(){
        return $this->belongsToMany(Log::class);
    }


}
