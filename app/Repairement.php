<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Repairement extends Model
{
    //
    public function machine(){
        return $this->belongsTo(Machine::class);
    }

    public function company(){
        return $this->belongsTo(Company::class);
    }

    public function log(){
        return $this->belongsToMany(Log::class);
    }
}
