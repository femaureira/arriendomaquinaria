<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    //
    public function rent(){
        return $this->belongsToMany(Rent::class);
    }
    public function machine(){
        return $this->belongsToMany(Machine::class);
    }
}
