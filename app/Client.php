<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //
    public function log(){
        return $this->belongsToMany(Log::class);
    }

    public function rent(){
        return $this->belongsToMany(Rent::class);
    }
}
