<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Companies_type extends Model
{
    //
    public function company(){
        return $this->belongsTo(Company::class);
    }

    public function type(){
        return $this->belongsTo(Type::class);
    }
}
