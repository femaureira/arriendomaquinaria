<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    //
    public function log(){
        return $this->belongsToMany(Log::class);
    }
}
