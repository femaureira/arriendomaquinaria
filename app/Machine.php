<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Machine extends Model
{
    //
    public function type(){
        return $this->belongsTo(Type::class);
    }

    public function state(){
        return $this->hasOne(State::class);
    }

    public function location(){
        return $this->belongsTo(Location::class);
    }

    public function log(){
        return $this->belongsToMany(Log::class);
    }

    public function rent(){
        return $this->belongsToMany(Rent::class);
    }

    public function repairement(){
        return $this->hasMany(Repairement::class);
    }
}
