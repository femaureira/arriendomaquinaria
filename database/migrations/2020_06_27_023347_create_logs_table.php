<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->unsignedBigInteger('employee_id')->unsigned();
            $table->unsignedBigInteger('client_id')->unsigned();
            $table->unsignedBigInteger('machine_id')->unsigned();
            $table->unsignedBigInteger('type_id')->unsigned();
            $table->unsignedBigInteger('company_id')->unsigned();
            $table->unsignedBigInteger('rent_id')->unsigned();
            $table->unsignedBigInteger('repairement_id')->unsigned();
            $table->unsignedBigInteger('action_id')->unsigned();
            $table->string('name');
            $table->string('description');

            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('machine_id')->references('id')->on('machines');
            $table->foreign('type_id')->references('id')->on('types');
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('rent_id')->references('id')->on('rents');
            $table->foreign('repairement_id')->references('id')->on('repairements');
            $table->foreign('action_id')->references('id')->on('actions');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
