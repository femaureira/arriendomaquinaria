<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMachinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('machines', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->unsignedBigInteger('location_id')->unsigned();
            $table->unsignedBigInteger('state_id')->unsigned();
            $table->unsignedBigInteger('type_id')->unsigned();
            $table->string('name');
            $table->integer('number');
            $table->string('code');
            $table->string('description');


            $table->foreign('location_id')->references('id')->on('locations');
            $table->foreign('state_id')->references('id')->on('states');
            $table->foreign('type_id')->references('id')->on('types');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('machines');
    }
}
