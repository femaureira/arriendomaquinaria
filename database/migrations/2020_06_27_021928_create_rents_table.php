<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rents', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->unsignedBigInteger('client_id')->unsigned();
            $table->unsignedBigInteger('employee_id')->unsigned();
            $table->unsignedBigInteger('machine_id')->unsigned();
            $table->unsignedBigInteger('initialState_id')->unsigned();
            $table->unsignedBigInteger('endState_id')->unsigned();
            $table->date('dateStart');
            $table->date('dateEnd');
            $table->date('dateReturn');
            $table->integer('value');
            $table->string('description');

            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('machine_id')->references('id')->on('machines');
            $table->foreign('initialState_id')->references('id')->on('states');
            $table->foreign('endState_id')->references('id')->on('states');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rents');
    }
}
