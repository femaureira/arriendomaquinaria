<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepairementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repairements', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->unsignedBigInteger('machine_id')->unsigned();
            $table->unsignedBigInteger('company_id')->unsigned();
            $table->date('dateStart');
            $table->date('dateEnd');
            $table->string('description');
            $table->integer('price');

            $table->foreign('machine_id')->references('id')->on('machines'); 
            $table->foreign('company_id')->references('id')->on('companies');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repairements');
    }
}
