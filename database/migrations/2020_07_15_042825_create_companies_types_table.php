<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies_types', function (Blueprint $table) {
            $table->id()->autoIncrement();
            $table->unsignedBigInteger('type_id')->unsigned();
            $table->unsignedBigInteger('company_id')->unsigned();
            $table->string('description');
            $table->integer('number');

            $table->foreign('type_id')->references('id')->on('types');
            $table->foreign('company_id')->references('id')->on('companies');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies_types');
    }
}
